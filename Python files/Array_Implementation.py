# Common Arrays Implementation are showed in the next lines:

# append()                  to add element to the end of the list
# extend()                  to extend all elements of a list to the another list
# insert()                  to insert an element at the another index
# remove()                  to remove an element from the list
# pop()                     to remove elements return element at the given index
# clear()                   to remove all elements from the list
# index()                   to return the index of the first matched element
# count()                   to count of number of elements passed as an argument
# sort()                    to sort the elements in ascending order by default
# reverse()                 to reverse order element in a list
# copy()                    to return a copy of elements in a list

##########

# Defininng and declaring and array
arr = [10, 20, 30, 40, 50]
print(arr)

# Accessing array elements
print(arr[0])
print(arr[1])
print(arr[2])
print(arr[-1])
print(arr[-2])
print()

##########

brands = ["Coke", "Apple", "Google", "Microsoft", "Toyota"]
print(brands)

# Finding the length of the array
num_brands = len(brands)
print(num_brands)

# Adding an element to an array using append()
brands.append("Intel")
print(brands)
print()

##########

# Removing elements from an array
colors = ["violet", "indigo", "blue", "green", "yellow", "orange", "red"]
print(colors)

del colors[4]
print(colors)

colors.remove("blue")
print(colors)

colors.pop(3)
print(colors)
print()

##########

# Modifying elements of an array using indexing
fruits = ["Apple", "Banana", "Mango", "Grapes", "Orange"]
print(fruits)

fruits[1] = "Pineapple"
print(fruits)

fruits[-1] = "Guava"
print(fruits)
print()

##########

# Concatenating two arrays using the + operator
concat = [1, 2, 3]
print(concat)

concat = concat + [4, 5, 6]
print(concat)
print()

##########

# Repeating element in an array
repeat = ["a"]
print(repeat)

repeat = repeat * 5
print(repeat)
print()

##########

# Slicing an array
fruits = ["Apple", "Banana", "Mango", "Grapes", "Orange"]
print(fruits)
print(fruits[1:4])
print(fruits[ :3])
print(fruits[-4:])
print(fruits[-3:-1])
print()

##########

# Declaring and defining multidimensional arrays
multd = [[1, 2], [3, 4], [5, 6], [7, 8]]
print(multd)
print(multd[0])
print(multd[3])
print(multd[2][1])
print(multd[3][0])
