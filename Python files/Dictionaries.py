# Dictionary is an unordered collection of items.
# A dictionary has a key.

# Example:
# my_dict = {1: 'red', 2: 'yellow', 3: 'violet'}

# clear()                           Remove all items from the dictionary
# copy()                            Return a shallow copy of the dictionary
# fromkeys(seg[,v])                 Return a new dictionary with keys from seq and value equal to v (default to None)
# get(key[,d])                      Return the value of key. If key doesn't exist, return d (defaults to None)
# items()                           Return a new view of the dictionary's items (key, value)
# keys()                            Return a new view of the dictionary's keys
# pop(key[,d])                      Remove the item with key and return its value or d if key is not found.
# If d is not provided and key is not found, raises KeyError
# popitem()                         Remove and return an arbitrary item (key, value).
# Raises KeyError if the dictionary is empty
# setdefault(key[,d])               If key is in the dictionary, return its value. If not, insert key with a value
# of d and return d (defaults to None)
# update([other])                   Update the dictionary with the key/value pairs from other, overwriting existing keys
# values()                          Return a new view of the dictionary's values

# Accessing elements from a dictionary
new_dict = {1: "Hello", 2:"Hi", 3:"Hola"}
print(new_dict)
print(new_dict[1])
print(new_dict.get(2))

# Updating value
new_dict[1] = "Hey"
print(new_dict)

# Adding value
new_dict[4] = "Namaste"
print(new_dict)
print()

##########

# Creating a new dictionary
squares = {1: 1, 2: 4, 3: 9, 4: 16, 5: 25}
print(squares)

# remove a particular item
print(squares.pop(4))
print(squares)

# remove an arbitrary item
print(squares.popitem())
print(squares)

# delete a particular item
del squares[1]

print(squares)
print()

##########

# Creating a new dictionary
squares = {1: 1, 2: 4, 3: 9, 4: 16, 5: 25}
print(squares)

# remove all items
squares.clear()

# Output: {}
print(squares)

# delete the dictionary itself
del squares

# Throws Error as the dictionary has been deleted
# print(squares)
print()

##########

# Creation a new dictionary using Comprehension
squares = {x: x*x for x in range(6)}
print(squares)

# Dictionary Membership test
squares = {1: 1, 3: 9, 5: 25, 7: 49, 9: 81}

print(1 in squares)

print(2 not in squares)

# membership tests for key only not value
print(49 in squares)
print()

##########

# Iterating through a dictionary
squares = {1: 1, 3: 9, 5: 25, 7: 49, 9: 81}
for i in squares:
    print(squares[i])

# Using built-in functions in a dictionary
squares = {1: 1, 3: 9, 5: 25, 7: 49, 9: 81}

print(len(squares))             # Prints the length of the dictionary

print(sorted(squares))          # Prints the dictionary in sorted order
