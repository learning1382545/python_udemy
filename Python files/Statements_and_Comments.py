# Statement: instructions that a Python interpreter can execute ==> if, for, while...
# Multi-line statement: We can make a statement extend over multiple lines with the line continuation character (\)
# Comments: are only commented by using (#). If a multi-line is needed, use the """   """
# Docstring: it is a string that occurs as the first statement in a module, function, class or method definition.

# This is a comment

# This is a long comment
# and multi-line comments
# can be written like this

"""
Multi-line comments
can also be written like this
"""

##########

# Statement - Assignment Statement
a = 1

##########

# Multiline Statement
# Explicit line continuation - '\'
b = 1 + 2 + 3 + \
    4 + 5 + 6
# Implicit line continuation within brackets
c = (1 + 2 + 3 +
     4 + 5 + 6)

##########

# Multiple statements in one line using - ';'
d = 1; e = 3; f = 0

##########

# Code block (body of a function, loop, etc.) starts with indentation
# and ends with the first unindented line.
for i in range(1, 10):
    print(i)
    if i == 5:
        break
print("End of program...")
