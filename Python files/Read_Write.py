# 'r' ==> open a file for reading (default)
# 'w' ==> open a file for writing. Creates a new file if it does not exist or truncates the file if it exists
# 'x' ==> open a file for exclusive creation. If the file already exists, the operation fails
# 'a' ==> open for appending at the end of the file without truncating it. Creates a new file if it does not exist
# 't' ==> open in text mode (default)
# 'b' ==> open in binary mode
# '+' ==> open a file for updating (reading and writing)
