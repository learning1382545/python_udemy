try:
    """
    The code which can give rise to an exception is written here.
    """
    a = "hi"
    b = int(a)
except:
    print("Exception caught!")
print()

##########

# Catching specific exception
try:
    a = 5
    b = 0
    c = a / b
except ZeroDivisionError:
    print("Division by zero is not possible")
print()

##########

# Exceptions can be raised also
try:
    raise TypeError
except TypeError:
    print("TypeError Exception caught!")
print()

##########

# try... finally
try:
    print("In try block")
    raise TypeError                 # Comment this line to confirm its functioning
except:
    print("In except block")
finally:
    print("In the finally block")
