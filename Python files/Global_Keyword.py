# global keyword allows us to modify the variable outside of the current scope. It is used to create a global variable
# and make changes to the variable in a local context.

def funct1():
    x = 20

    def funct2():
        global x               # global keyword is used to modify a global variable
        x = 25

    print("Before calling funct2: ", x)
    print("Calling funct2 now")
    funct2()
    print("After calling funct2: ", x)


funct1()
print("x in main: ", x)
