# Keywords are reserved words in Python
# List of Python keywords
# False     class       finally is          return
# None      continue    for     lambda      try
# True      def         from    nonlocal    while
# and       del         global  not         with
# as        elif        if      or          yield
# assert    else        import  pass
# break     except      in      raise

# Python identifier: names given to entities like class, functions, variables, etc

##########

# True False
print(5 == 5)
print(5 > 5)
print()

##########

# None
print(None == 0)
print(None == False)
print(None == [])
print(None == None)
print()


##########

# Functions
def a_void_function():
    a = 1
    b = 2
    c = a + b


x = a_void_function()
print(x)
print()

##########

# and or not
print(True and False)
print(True or False)
print(not False)
print()

##########

# as
import math as myMath

print(myMath.cos(myMath.pi))
print()

##########

# assert
# assert 5 > 5                    # as this gives an error, it is indicated when python is executed
assert 5 == 5
print()

##########

# break                         # break generates an ending of the if and for command
for i in range(1, 11):
    if i == 5:
        break
    print(i)
print()

##########

# continue                      # continue generates an ending of the if command
for i in range(1, 11):
    if i == 5:
        continue
    print(i)
print()


##########

# class
class ExampleClass:
    def function1(parameters):
        print("function1() is executing...")

    def function2(parameters):
        print("function2() is executing...")


ob1 = ExampleClass()
ob1.function1()
ob1.function2()
print()


##########

# def
def function_name(parameters):
    pass


##########

# del
a = 10
print(a)
del a
# print(a)            # generates an error... a was deleted
print()

##########

# if... elif... else
num = 4
if num == 1:
    print('One')
elif num == 2:
    print('Two')
elif num == 3:
    print('Three')
else:
    print('Something else')
print()

##########

# try... raise... catch... finally
try:
    x = 9
    raise ZeroDivisionError
except ZeroDivisionError:
    print("Division cannot be performed")
finally:
    print("Execution Successfully")
print()

##########

# for
for i in range(1, 10):
    print(i)
print()
for i in range(1, 10, 2):
    print(i)
print()

##########

# from... import
import math
from math import cos

##########

# global
globvar = 10


def read1():
    print(globvar)


def write1():
    global globvar
    globvar = 5


def write2():
    globvar = 15


read1()
write1()
read1()
write2()
read1()
print()

##########

# in
a = [1, 2, 3, 4]
print(4 in a)
print()

##########

# is
print(True is True)
print()

##########

# lambda                    # anonymous function
a = lambda x: x*2
for i in range(1, 6):
    print(a(i))
print()

##########

# nonlocal
def outer_function():
    a = 5
    def inner_function():
        nonlocal a
        print("First call: ", a)
        a = 10
        print("Inner function: ", a)
    inner_function()
    print("Outer function: ", a)

outer_function()
print()

##########

# pass
def function(args):
    pass

##########

# return
def func_return():
    a = 10
    return a


print(func_return())
print()

##########

# while
i = 5
while i > 0:
    print(i)
    i -= 1
print()

##########

# with

with open('example.txt', 'w') as my_file:
    my_file.write('Hello world!')

##########


# yield                     # accumulate all the calculated values
def generator():
    for i in range(6):
        yield i*i


g = generator()
for i in g:
    print(i)
print()
