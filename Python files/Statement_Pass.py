# In Python, pass is a null statement.
# The difference between a comment and pass statement in Python is that, while the interpreter ignores a comment
# entirely, pass is not ignored.

# pass is just a placholder for functionality to be added later
sequence = {'p', 'a', 's', 's'}
for val in sequence:
    pass
print("End of the program...")
