# Generators are a simple way of creating iterators.
# A generator is a function that returns an object (iterator), which we can iterate over (one value at time).

# A simple generator function
def my_generator():
    n = 1
    print('This is printed first')
    # Generator function contains yield statements
    yield n

    n += 1
    print('This is printed second')
    yield n

    n += 1
    print('This is printed at last')
    yield n

##########


a = my_generator()
# Iterating using next
next(a)
next(a)
next(a)
print()
print("Using for loop...")
# Iterating using for loop
for item in my_generator():
    print(item)
print()

##########


# Generators with a loop
def reverse_string(my_string):
    length = len(my_string)
    for i in range(length - 1, -1, -1):
        yield my_string[i]

# For loop to reverse the string


for char in reverse_string("WORLD"):
    print(char)
