# Argument passing to a function is optional. A function may or may not have arguments passed.

def findMax(a, b):
    if a > b:
        return a
    else:
        return b


print("Max number between 10 and 20 is", findMax(10, 20))
print()

##########


def hello(name, msg=", how are you?"):  # Function with default parameter
    print("Hello ", name, msg)


hello("Agnibha", ", have a nice day.")
hello("Agnibha")

##########


def sumAll(*args):                      # Function with arbitrary arguments
    sum = 0
    for i in args:
        sum += i
    return sum


print("Sum of all the integers between 1-5 is ", sumAll(1, 2, 3, 4, 5))
print()

##########


def defaultArg(a, b, c):
    print("a = {}, b = {}, c = {}...".format(a, b, c))


defaultArg(10, 20, 30)
print()
defaultArg(b=222, c=333, a=111)
