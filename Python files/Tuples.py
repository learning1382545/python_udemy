# Tuples: in many aspects a tuple is similar to a list.
# Basic difference:
# We cannot change the elements of a tuple once it is assigned whereas in a list, elements can be changed.

# built-in functions with tuples:
# all()                 Return True if all elements of the tuple are true (or if the tuple is empty)
# any()                 Return True if any element of the tuple is true. If the tuple is empty, return False
# enumerate()           Return an enumerate object. It contains the index and value of all the items of tuples as pairs
# len()                 Return the length (the number of items) in the tuple
# max()                 Return the largest item in the tuple
# sorted()              Take elements in the tuple and return a new sorted list (does not sort the tuple itself)
# sum()                 Return the sum of all elements in the tuple
# tuple()               Convert an iterable (list, string, set, dictionary) to a tuple

##########

# creating an empty tuple
tuple1 = ()
print(tuple1)

# creating tuples with integer elements
tuple2 = (1, 2, 3)
print(tuple2)

# tuple with mixed datatypes
tuple3 = (101, "Anirban", 20000.00, "HR Dept")
print(tuple3)
print()

##########

# creation of nested tuple
tuple4 = ("points", [1, 4, 3], (7, 8, 6))
print(tuple4)

# tuple can be created without any parentheses
# also called tuple packing
tuple5 = 101, "Anirban", 20000.00, "HR Dept"
print(tuple5)

# tuple unpacking is also possible
empid, empname, empsal, empdept = tuple5
print(empid)
print(empname)
print(empsal)
print(empdept)

print(type(tuple5))
print()

##########

# accessing elements in a tuple
tuple1 = ('w', 'e', 'l', 'c', 'o', 'm', 'e')
print(tuple1)
print(tuple1[1])
print(tuple1[3])
print(tuple1[5])

# nested tuple
nest_tuple2 = ("point", [1, 3, 4], [8, 7, 9])
print(nest_tuple2)
print(nest_tuple2[0][3])
print(nest_tuple2[1][2])
print(nest_tuple2[2][2])
print()

##########

# slicing tuple contents
tuple1 = ('w', 'e', 'l', 'c', 'o', 'm', 'e')

print(tuple1[1:3])

print(tuple1[:-3])

print(tuple1[3:])

print(tuple1[:])
print()

##########

# tuple elements are immutable
tuple1 = ('w', 'e', 'l', 'c', 'o', 'm', 'e')
print(tuple1)
# tuple1[2] = 'x'               # error

# tuples can be reassigned
tuple1 = ('g', 'o', 'o', 'd', 'b', 'y', 'e')
print(tuple1)
print()

##########

# concatenation of tuples
tuple2 = ('w', 'e', 'l')
tuple3 = ('c', 'o', 'm', 'e')
print(tuple2)
print(tuple3)
print(tuple2 + tuple3)

print(("again",) * 4)
print()

##########

# deletion operation on a tuple
tuple4 = ('w', 'e', 'l', 'c', 'o', 'm', 'e')

# as immutable so elements can not be deleted
# del tuple4[2]                     # this will generate an error

# but can delete entire tuple
del tuple4

##########

# tuple methods
tuple5 = ('w', 'e', 'l', 'c', 'o', 'm', 'e')
print(tuple5.count('e'))
print(tuple5.index('c'))
print()

##########

# tuple operations
tuple6 = ('w', 'e', 'l', 'c', 'o', 'm', 'e')

# membership
print('c' in tuple6)
print('c' not in tuple6)
print('a' in tuple6)
print('a' not in tuple6)

##########

# iteration through tuple elements
tuple6 = ('w', 'e', 'l', 'c', 'o', 'm', 'e')
for letters in tuple6:
    print("letter is -> ", letters)
print()

##########

# built-in functions with tuple
tuple7 = (22, 33, 55, 44, 77, 66, 11)
print(tuple7)

print(max(tuple7))
print(min(tuple7))
print(sorted(tuple7))
print(len(tuple7))
