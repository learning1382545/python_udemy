# Closure is a function object that remembers values in enclosing scopes even if they are not present in memory

# Defining a closure function
def print_message(message):
    # This is the outer enclosing function

    def print_message_inner():
        # This is the nested function
        print(message)

    return print_message_inner  # this got changed


# Now let's try calling this function
# Output: Hello
another = print_message("Hello")
another()


##########

# Another demonstration of the closure function
def multiplier_outer(n):
    def multiplier_inner(x):
        return x * n

    return multiplier_inner


# Multiplier of 3
times3 = multiplier_outer(3)

# Multiplier of 5
times5 = multiplier_outer(5)

# Output: 27
print(times3(9))

# Output: 15
print(times5(3))

# Output: 30
print(times5(times3(2)))
