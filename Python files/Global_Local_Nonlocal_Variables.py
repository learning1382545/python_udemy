# Global variables: a variable which is declared outside of the function or in global scope is known as global variable
#                   this means, global variable can be accessed inside or outside of the function.
# Local variables:  a variable declared inside the function's body or in the local scope is known as local variable
# nonlocal variables: are used in nested functions whose local scope is not defined. This means, the variable can be
#                     neither in the local nor the global scope.

# global and local variable with different name
x = "global"  # global variable can be accessed from anywhere


def funct1():
    global x
    y = "local"
    x = x * 2
    print(x)
    print(y)


print("Global x = ", x)
funct1()
print("Global x = ", x)
print()

##########

# global and local variable with same name
a = 5


def funct2():
    a = 10  # local variables are accessed from the block where it is defined only
    print("local a: ", a)


print("global a: ", a)
funct2()
print("global a: ", a)
print()


##########

def outer():
    x = "local"

    def inner():
        nonlocal x  # Nonlocal variable are used in nested function
        x = "nonlocal"
        print("inner", x)

    inner()
    print("outer: ", x)


outer()
