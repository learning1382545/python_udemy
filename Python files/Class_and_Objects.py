# Object is a simply collection of data and methods
# Class is a blueprint for the object
# Object is also called an instance of a class and the process of creating this object is called instantiation.

# Example of a class
class MyComplexNumber:
    # Constructor methods
    def __init__(self, real=0, imag=0):
        print("MyComplexNumber constructor executing...")
        self.real_part = real
        self.imag_part = imag

    def displayComplex(self):
        print("{0} + {1}j".format(self.real_part, self.imag_part))


##########

# Create a new object against MyComplexNumber class
cmplx1 = MyComplexNumber(40, 50)

# Calling displayComplex() function
# Output: 40+50j
cmplx1.displayComplex()

##########

# Create another object against MyComplexnumber class
# and create a new attribute 'new_attribute'
cmplx2 = MyComplexNumber(60, 70)
cmplx2.new_attribute = 80

# Output: (60, 70, 80)
print((cmplx2.real_part, cmplx2.imag_part, cmplx2.new_attribute))

##########

# but cmplx1 object doesn't have attribute 'new_attribute'
# AttributeError: 'MyCmplexNumber' object has no attribute 'new_attribute'
cmplx1.new_attribute

##########

# Deleting object attributes and the object
print(cmplx1)
del cmplx1.real_part
del cmplx1

##########

# as it was deleted, next line presents an error
print(cmplx1)
