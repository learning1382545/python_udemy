# Declaring and assigning value to constants
PI = 3.14
GRAVITY = 9.8

#######

# Declaring and assigning value to a variable
a = "Apple"
print(a)

# Changing value of a variable
a = "Aeroplane"
print(a)

a = 100
print(a)

# Assigning multiple values to a variable
b, c, d = 1, 2.5, "Hello"
print(b, c, d)

# Assigning same value to multiple variables
b = c = d = 5
print(b, c, d)
