# Regular expressions allow you to locate and change
# strings in very powerful ways.
# They work in almost exactly the same way in every
# programming language as well.

# Regular Expressions (Regex) are used to
# 1_ Search for a specific string in a large amount of data
# 2_ Verify that a string has the proper format (Email, Phone #)
# 3_ Find a string and replace it with another string
# 4_Format data into the proper form for importing for example

# import the Regex module
import re

# Search for ape in the string - one match
if re.search("ape", "The ape was at the apex"):
    print("There is an ape")
print()

##########

import re
# Get all matches

# findall() returns a list of matches
# . is used to match any 1 character or space
allapes = re.findall("ape.", "The ape was at the apex")

for i in allapes:
    print(i)
print()

##########

# finditer returns an iterator of matching objects
# you can use span to get the location

thestr = "The ape was at the apex"

for i in re.finditer("ape.", thestr):
    # span returns a tuple
    locTuple = i.span()

    print(locTuple)

    # slice the match out using the tuple values
    print(thestr[locTuple[0]:locTuple[1]])
print()

##########

import re
# match 1 of several letters

# square brackets will match anyone of the characters between
# the brackets not including upper and lowercase varieties
# unless they are listed

animalstr = "Cat rat mat fat pat"

allanimals = re.findall("[crmfp]at", animalstr)
for i in allanimals:
    print(i)

print()

##########

# We can also allow for characters in a range
# remember to include upper and lowercase letters
animalstr = "Cat rat mat fat pat"
someanimals = re.findall("[c-mC-M]at", animalstr)
for i in someanimals:
    print(i)

print()

##########

# Use ^ to denote any character but whatever characters are between the brackets
animalstr = "Cat rat mat fat pat"
someanimals = re.findall("[^Cr]at", animalstr)
for i in someanimals:
    print(i)
print()

##########

import re
# Replace all matches

owlfood = "rat cat mat pat"

# you can compile  regex into pattern objects which provide additional methods
regex = re.compile("[cr]at")

# sub() replaces items that match the regex in the string with the 1st attribute string passed to sub
owlfood = regex.sub("owl", owlfood)

print(owlfood)
print()

##########

import re
# solving backslash problems

# regex use the backslash to designate special characters and Python does the same inside strings which causes issues

# Let's try to get "\\stuff" out of a string

randstr = "Here is \\stuff"

# This won't find it
print("Find \\stuff : ", re.search("\\stuff", randstr))

# This does, but we have to put in 4 slashes which is messy
print("Find \\stuff : ", re.search("\\\\stuff", randstr))

# You can get around this by using raw strings which don't treat backslashes as special
print("Find \\stuff : ", re.search(r"\\stuff", randstr))
print()

##########

import re
# Matching any character

# We saw that . matches any character, but what if we want to match a period. Backslash the period
# You do the same with [, ] and others

randstr = "F.B.I. I.R.S. CIA"

print("Matches :", len(re.findall(".\..\..", randstr)))
print("Matches :", re.findall(".\..\..", randstr))
print()

##########

import re
# Matching whitespace
# We can match many whitespace characters

randstr = """This is a long
string that goes
on for many lines"""

print(randstr)

# Remove newlines
regex = re.compile("\n")

randstr = regex.sub(" ", randstr)

print(randstr)
print()

# You can also match
# \b : backspace
# \f : form feed
# \r : carriage return
# \t : tab
# \v : vertical tab

# you may need to remove \r\n on Windows

##########

import re
# Matching any single number
# \d can be used instead of [0-9]
# \D is the same as [^0-9]

randstr = "12345"

print("Matches :", len(re.findall("\d", randstr)))
print("Matches :", re.findall("\d", randstr))
print()

##########

import re
# Matching multiple numbers
# you can match multiple digits by following the \d with {numOfValues}

# Match 5 numbers only
if re.search("\d{5}", "12345"):
    print("It is a zip code")

# You can also match within a range
# Match values that are between 5 and 7 digits
numStr = "123 12345 123456 1234567"

print("Matches :", len(re.findall("\d{5,7}", numStr)))
print()

##########

import re
# matching any single letter or number
# \w is the same as [a-zA-Z0-9_]
# \W is the same as [^a-zA-Z0-9_]

phNum = "412-555-1212"

# Check if it is a phone number
if re.search("\w{3}-\w{3}-\w{4}", phNum):
    print("It is a phone number")

# Check for valid first name between 2 and 20 characters
if re.search("\w{2,20}", "Ultraman"):
    print("It is a valid name")

print()

##########

import re
# Matching Whitespace
# \s is the same as [\f\n\r\t\v]
# \S is the same as [^\f\n\r\t\v]

# Check for valid first and last name with a space
if re.search("\w{2,20}\s\w{2,20}", "Toshio Muramatsu"):
    print("It is a valid full name")

print()
##########

import re
# Matching one or more

# Match a followed by 1 or more characters
print("Matches :", len(re.findall("a+", "a as ape bug")))
print("Matches :", re.findall("a+", "a as ape bug"))

print()

##########

import re
# Situation:
# Create a regex that matches email addresses from a list
# 1. 1 to 20 lowercase and uppercase letters, numbers, plus ._%+-
# 2. An @ symbol
# 3. 2 to 20 lowercase and uppercase letters, numbers, plus .-
# 4. A period
# 5. 2 to 3 lowercase and uppercase letters

emailList = "db@aol.com m@.com @apple.com db@.com"

print("Email Matches :", len(re.findall("[\w._%+-]{1,20}@[\w.-]{2,20}.[A-Za-z]{2,3}", emailList)))
print("Email Matches :", re.findall("[\w._%+-]{1,20}@[\w.-]{2,20}.[A-Za-z]{2,3}", emailList))
