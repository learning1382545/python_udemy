# Is a Python source file, which can expose classes, functions and global variables.
# As an example:
# import numpy
# import numpy as np

# Importing module as well as remaining it
import math as m
print("The value of pi is", m.pi)

##########

# Usage of from
# import math from pi
# print("The value of pi is", pi)

##########

# Import all names from the standard module math
# from math import *
# print("The value of pi is", pi)
