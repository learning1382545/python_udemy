# A directory or folder is a collection of set or files and sub directories.

import os
print(os.getcwd())              # Returns the present working directory
print(os.getcwdb())             # Returns the present working directory as a byte object

##########

os.chdir('E:\\Temporal\\Escuela\\Frances')  # use to change directoy
print(os.listdir())                         # All files and sub directories inside a directory can be known
                                            # using the listdir() method

##########

# used to make a new directory
os.mkdir('Test')

##########

# used to rename a directory
os.rename('Test', 'New_One')

##########

# Removing a file and directory
os.remove('Test.txt')
os.rmdir('New_One')
